function square(data, holder, which) {
  this.data = data;
  this.holder = holder;
  this.state = 0;
  this.tileDiv = null;
  this.hasClicked = false;
  this.createSquare.apply(this);
};

square.prototype.createSquare = function () {
  this.tileDiv = document.createElement("div");

  for (var item in this.data) {
    this.tileDiv.style[item] = this.data[item];
  }

  this.holder.append(this.tileDiv);
};

square.prototype.tileChecker = function (which) {
  if (!this.hasClicked) {
    if (which === "o") {
      this.tileDiv.style.background = "url(naught.png) no-repeat  center";
    } else if (which === "x") {
      this.tileDiv.style.background = "url(cross.jpg) no-repeat  center";
    }
  }
  this.state = which;
  this.hasClicked = true;
};

