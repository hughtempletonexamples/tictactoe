function tictactoe(data){
    this.data = data;
    this.holder = this.gameDiv;
    this.squareArray = [];
    this.players = [];
    this.turn = 0;
    this.counter = 0;
    this.createPlayer.apply(this);
    this.createGrid.apply(this);
    this.isWin = [];
    this.dimension = 3;
    this.targets = [];
    this.targets["x"] = this.dimension;
    this.targets["o"] = -Math.abs(this.dimension);
    this.targets[0] = 0;
    this.test = 0;
};

tictactoe.prototype.createGrid = function(){
    
    this.gameDiv = document.createElement("div");
    for (var element in this.data.gameWindow) {
        this.gameDiv.style[element] = this.data.gameWindow[element];
    }
    document.body.append(this.gameDiv);
    
    for (var element in this.data.squares) {
        
        var that = this;
        
        (function (that, element) {
            var box = new square(that.data.squares[element],that.gameDiv);
            box.tileDiv.addEventListener("click", function (){
                var person = that.players.pop();
                person.turn = true;
                box.tileChecker(person.marker);
                that.turn = person;
                that.players.unshift(person);
//                that.winPoss[box.data.id] = that.turn.marker;
                that.checkWin.apply(that);
                
            }.bind(that));
            
            that.squareArray.push(box);
        }(that, element));
        
    };
    
};

tictactoe.prototype.createPlayer = function () {
  
  for (var element in this.data.players) {
    var person = new player(this.data.players[element]);
    this.players.push(person);
  }
  
};

tictactoe.prototype.checkWin = function () {

  //build win matrix
  var holder = this.dimension;
  var total = this.dimension * this.dimension;
  var checker = 0;
  var sum = 0;
  var winMatrix = [];
  var lineArray = [];
  var column = [];
  var test = this.test;
  while (checker < holder) {
    if (checker === total) {
      break;
    }
    lineArray.push(this.targets[this.squareArray[checker].state]);
    if (checker === holder - 1) {
      holder = holder + this.dimension;
      winMatrix.push(lineArray);
      lineArray = [];

    }
    checker++;
  }

  //row win
  var sum = 0;
  for (var i = 0; i < winMatrix.length; i++) {
    var row = winMatrix[i];
    for (var j = 0; j < row.length; j++) {
      sum += row[j];
    }
    if (sum === total) {
      console.log("x win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    if (sum === -Math.abs(total)) {
      console.log("o win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    sum = 0;
  }
  //column win
  var sum = 0;

  for (var i = 0; i < winMatrix.length; i++) {
    for (var j = 0; j < winMatrix.length; j++) {
      sum += winMatrix[j][i];
    }
    if (sum === total) {
      console.log("x win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    if (sum === -Math.abs(total)) {
      console.log("o win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    sum = 0;
  }

  this.test++;
  //diagonally win
  var sum = 0;
  for (var i = 0; i < winMatrix.length; i++) {
    var row = winMatrix[i];
    sum += row[i];

    if (sum === total) {
      console.log("x win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    if (sum === -Math.abs(total)) {
      console.log("o win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
  }
  var real = winMatrix.length - 1;
  var sum = 0;
  var acc = 0;
  for (var i = real; i >= 0; i--) {

    sum += winMatrix[i][acc];

    if (sum === total) {
      console.log("x win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }
    if (sum === -Math.abs(total)) {
      console.log("o win");
      document.getElementById("checkGo").innerHTML = "the winner is : " + this.turn.data.id;
    }

    acc++;
  }
  
};    



